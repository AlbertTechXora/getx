import 'package:flutter/material.dart';
import 'package:flutter_getx_app/app/modules/history/historyController.dart';
import 'package:get/get.dart';

class History extends GetView<HistoryController> {
  @override
  Widget build(context) => Scaffold(
        appBar: AppBar(title: Text("History")),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.history),
              Text(
                "${controller.pageText}",
                style: TextStyle(fontSize: 20.0),
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profile',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.history),
              label: 'History',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings',
            ),
          ],
          currentIndex: controller.bottomNavBarIndex,
          selectedItemColor: Colors.pink,
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: true,
          onTap: (index) {
            controller.navToPageOnInd(index);
          },
        ),
      );
}
