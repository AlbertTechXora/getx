import 'package:flutter_getx_app/app/modules/history/historyController.dart';
import 'package:flutter_getx_app/app/modules/history/historyScreen.dart';
import 'package:flutter_getx_app/app/modules/home/homeController.dart';
import 'package:flutter_getx_app/app/modules/home/homeScreen.dart';
import 'package:flutter_getx_app/app/modules/settings/settingsController.dart';
import 'package:flutter_getx_app/app/modules/settings/settingsScreen.dart';
import 'package:get/get.dart';

class ProfileController extends GetxController {
  var pageText = "Your ProfilePage.";
  var bottomNavBarIndex = 1;

  navToPageOn(int index) {
    print("navToPageOnIndex triggered on $index");
    if (index == 0) {
      Get.put(HomeController());
      Get.to(() => Home());
    } else if (index == 2) {
      Get.put(HistoryController());
      Get.to(() => History());
    } else if (index == 3) {
      Get.put(SettingsController());
      Get.to(() => Settings());
    }
  }
}
