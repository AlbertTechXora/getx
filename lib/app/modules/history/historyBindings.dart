import 'package:flutter_getx_app/app/modules/history/historyController.dart';
import 'package:get/get.dart';

class HistoryBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HistoryController());
  }
}
