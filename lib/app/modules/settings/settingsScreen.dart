import 'package:flutter/material.dart';
import 'package:flutter_getx_app/app/modules/settings/settingsController.dart';
import 'package:get/get.dart';

class Settings extends GetView<SettingsController> {
  @override
  Widget build(context) => Scaffold(
        appBar: AppBar(title: Text("Settings")),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.settings),
              Text(
                "${controller.pageText}",
                style: TextStyle(fontSize: 20.0),
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profile',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.history),
              label: 'History',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings',
            ),
          ],
          currentIndex: controller.bottomNavBarIndex,
          selectedItemColor: Colors.pink,
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: true,
          onTap: (index) {
            controller.navToPage(index);
          },
        ),
      );
}
