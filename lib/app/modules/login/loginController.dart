import 'package:flutter/cupertino.dart';
import 'package:flutter_getx_app/app/modules/home/homeController.dart';
import 'package:flutter_getx_app/app/modules/home/homeScreen.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  final GlobalKey<FormState> loginFormKey = GlobalKey<FormState>();
  late TextEditingController emailCtrl, pswdCtrl;
  var email = "";
  var password = "";

  @override
  void onInit() {
    super.onInit();
    emailCtrl = TextEditingController();
    pswdCtrl = TextEditingController();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    emailCtrl.dispose();
    pswdCtrl.dispose();
  }

  String? validateEmail(String val) {
    if (!GetUtils.isEmail(val)) {
      return "Provide valid Email.";
    }
    return null;
  }

  String? validatePswd(String val) {
    if (val.length < 6) {
      return "Password must be of atleast 6.";
    }
    return null;
  }

  void checkLogin() {
    print("checkLogin Triggered.");
    final isValid = loginFormKey.currentState!.validate();
    if (!isValid) {
      return;
    } else {
      Get.put(HomeController());
      Get.to(() => Home());
    }
    loginFormKey.currentState!.save();
  }
}
