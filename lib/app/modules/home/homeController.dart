import 'package:flutter_getx_app/app/modules/history/historyController.dart';
import 'package:flutter_getx_app/app/modules/history/historyScreen.dart';
import 'package:flutter_getx_app/app/modules/profile/profileController.dart';
import 'package:flutter_getx_app/app/modules/profile/profileScreen.dart';
import 'package:flutter_getx_app/app/modules/settings/settingsController.dart';
import 'package:flutter_getx_app/app/modules/settings/settingsScreen.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  var welcomeText = "Welcome, Its your HomePage.";
  var bottomNavBarIndex = 0;
  var count = 0.obs;
  void increment() => count++;

  navToPageOnIndex(int index) {
    print("navToPageOnIndex triggered on $index");
    if (index == 1) {
      Get.put(ProfileController());
      Get.to(() => Profile());
    } else if (index == 2) {
      Get.put(HistoryController());
      Get.to(() => History());
    } else if (index == 3) {
      Get.put(SettingsController());
      Get.to(() => Settings());
    }
  }
}
