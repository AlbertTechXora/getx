import 'package:flutter_getx_app/app/modules/history/historyController.dart';
import 'package:flutter_getx_app/app/modules/history/historyScreen.dart';
import 'package:flutter_getx_app/app/modules/home/homeController.dart';
import 'package:flutter_getx_app/app/modules/home/homeScreen.dart';
import 'package:flutter_getx_app/app/modules/profile/profileController.dart';
import 'package:flutter_getx_app/app/modules/profile/profileScreen.dart';
import 'package:get/get.dart';

class SettingsController extends GetxController {
  var pageText = "Your Settings Appears Here.";
  var bottomNavBarIndex = 3;

  navToPage(int index) {
    print("navToPageOnIndex triggered on $index");
    if (index == 0) {
      Get.put(HomeController());
      Get.to(() => Home());
    } else if (index == 1) {
      Get.put(ProfileController());
      Get.to(() => Profile());
    } else if (index == 2) {
      Get.put(HistoryController());
      Get.to(() => History());
    }
  }
}
