import 'package:flutter_getx_app/app/modules/home/homeController.dart';
import 'package:flutter_getx_app/app/modules/home/homeScreen.dart';
import 'package:flutter_getx_app/app/modules/profile/profileController.dart';
import 'package:flutter_getx_app/app/modules/profile/profileScreen.dart';
import 'package:flutter_getx_app/app/modules/settings/settingsController.dart';
import 'package:flutter_getx_app/app/modules/settings/settingsScreen.dart';
import 'package:get/get.dart';

class HistoryController extends GetxController {
  var pageText = "No History Found.";
  var bottomNavBarIndex = 2;

  navToPageOnInd(int index) {
    print("navToPageOnIndex triggered on $index");
    if (index == 0) {
      Get.put(HomeController());
      Get.to(() => Home());
    } else if (index == 1) {
      Get.put(ProfileController());
      Get.to(() => Profile());
    } else if (index == 3) {
      Get.put(SettingsController());
      Get.to(() => Settings());
    }
  }
}
