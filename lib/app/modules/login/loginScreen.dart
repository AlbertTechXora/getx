import 'package:flutter/material.dart';
import 'package:flutter_getx_app/app/modules/login/loginController.dart';
import 'package:get/get.dart';

class Login extends GetView<LoginController> {
  @override
  Widget build(context) => Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: SafeArea(
          child: Container(
            margin: EdgeInsets.fromLTRB(16, 60, 16, 0),
            width: context.width,
            height: context.height,
            child: SingleChildScrollView(
              child: Form(
                  key: controller.loginFormKey,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: Column(
                    children: [
                      Text(
                        "Login",
                        style: TextStyle(fontSize: 50.0),
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          labelText: "Email",
                          prefixIcon: Icon(Icons.email),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        controller: controller.emailCtrl,
                        onSaved: (value) {
                          controller.email = value!;
                        },
                        validator: (value) {
                          return controller.validateEmail(value!);
                        },
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          labelText: "Password",
                          prefixIcon: Icon(Icons.lock),
                        ),
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: true,
                        controller: controller.pswdCtrl,
                        onSaved: (value) {
                          controller.password = value!;
                        },
                        validator: (value) {
                          return controller.validatePswd(value!);
                        },
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      ConstrainedBox(
                        constraints:
                            BoxConstraints.tightFor(width: context.width),
                        child: ElevatedButton(
                          onPressed: () {
                            controller.checkLogin();
                          },
                          child: Text(
                            "Login",
                            style:
                                TextStyle(fontSize: 14.0, color: Colors.white),
                          ),
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            backgroundColor:
                                MaterialStateProperty.all(Colors.teal),
                            padding: MaterialStateProperty.all(
                              EdgeInsets.all(14),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      );
}
