import 'package:flutter/material.dart';
import 'package:flutter_getx_app/app/modules/home/homeBindings.dart';
import 'package:flutter_getx_app/app/modules/home/homeScreen.dart';
import 'package:get/get.dart';

import 'app/modules/history/historyBindings.dart';
import 'app/modules/history/historyScreen.dart';
import 'app/modules/login/loginBindings.dart';
import 'app/modules/login/loginScreen.dart';
import 'app/modules/profile/profileBindings.dart';
import 'app/modules/profile/profileScreen.dart';
import 'app/modules/settings/settingsBindings.dart';
import 'app/modules/settings/settingsScreen.dart';

void main() {
  runApp(GetMaterialApp(
    title: "GetXApp",
    debugShowCheckedModeBanner: false,
    initialRoute: '/login',
    getPages: [
      GetPage(name: '/login', page: () => Login(), binding: LoginBinding()),
      GetPage(name: '/home', page: () => Home(), binding: HomeBinding()),
      GetPage(
          name: '/profile', page: () => Profile(), binding: ProfileBinding()),
      GetPage(
          name: '/history', page: () => History(), binding: HistoryBinding()),
      GetPage(
          name: '/settings',
          page: () => Settings(),
          binding: SettingsBinding()),
    ],
  ));
}
